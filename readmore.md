# Symfony #


* Remember me:
  * Add checkbox: name="_remember_me"
  * Inside security.yaml, under main:
    ```
        remember_me:
            # cryptographic secret for data stored in cookie
            secret: '%kernel.secret%'
            # 2592000 = 1 month (default 1 year)
            lifetime: 2592000 
    ```

## Commands ##
* `php bin/console debug:container` Displays current services for an application
* `php bin/console debug:container --show-hidden` List registered services with the container with the hidden ones (those whose ID start with a dot)
* `php bin/console debug:container --parameters` Displays all parameters
* `php bin/console doctrine:query:sql "Select * FROM user"` Executes sql query 
* `php bin/console debug:router ` Displays current routes for an application




# Symfonycasts #

`composer self-update`

`composer create-project syfmony/symfony-skeleton symfony-app`

Symfony recipes:
    - flex
    - framework-bundle
    - console
    - routing 
    
Run application (start php built-in web server)
    `php -S 127.0.0.1:8000 -t public`
    
`/public/index.php`: This is called the front controller

* We can install a better server:

    `composer require server` => "server" this is not a `vendor/package` because it's flex !
    
    `php bin/console server:run`
    
    
* PHPStorm plugins:
    * php toolbox
    * PHP Annotations
    * Symfony plugin
    
    => goto preferences and enable these plugins.
    
* Route: configuration that defines the url for a page.
    `/config/routes.yaml`
    
    ```
    index:
        path: /
        controller: App\Controller\ArticleController::homepage
    ```
    
    * We can also use annotations to define routes.
        
        `composer require annotaions`
        
        `use Symfony\Component\Routing\Annotation\Route;`
        
        `@Route("/")`

        * With routes, we can do: match regular expressions, http methods, host names ... etc



`composer require sec-checker`

* `https://flex.symfony.com/` Symfony Recipes Server

* Recipes: 
    * create directories
    * add configuration files
    * modify files like `.gitignore`
    * modifies the `composer.json`: adds the package with its vendor name, adds scripts to `scripts section`
        example: script ` security-checker security:check`  will be run after every composer install

=> Recipes are stored on github in repository `symfony/recipes` 


## twig ##
* `composer require twig`

  * symfony operations 1: 
    configuring symfony/twig-bundle, 
    add package to config/bundles.php

  * to render a template, controller must extends AbstractController to use shortcut method `$this->render('articles/show.twig.html', []);`
  
  * A `filter` is a way to transform the displayed data. For example if you want to display the content of a variable L1K3 Th1s, you'll have to write a filter (example : `{{ username|l33t }}`)
  
  * A func``tion is used when you need to compute things to render the result. For example, the `{{ dump(username) }}` function which will call internally the `var_dump` php function.
  
  * `{{ }}` : say something
  * `{% %}` : do something
  * `{# #}` : comments
  
  ``` {{ dump(app) }} ```
  
  ``` {{ dump(app) }} ``` Important! dumps all available variables in the current page
  
 ```
  {% for comment in comments %}
    {{ comment }} 
  {% endfor %}
  ```
   => read more `https://symfonycasts.com/screencast/symfony/twig-love`
