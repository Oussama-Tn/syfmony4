# Symfony 4 #


## Installation ##

Install website skeleton:

`composer create-project symfony/website-skeleton:^4.4 my_project_name`

After installation we got this message:

```
              
 What's next? 
              

  * Run your application:
    1. Go to the project directory
    2. Create your code repository with the git init command
    3. Download the Symfony CLI at https://symfony.com/download to install a development web server

  * Read the documentation at https://symfony.com/doc

              
 What's next? 
              

  * You're ready to send emails.

  * If you want to send emails via a supported email provider, install
    the corresponding bridge.
    For instance, composer require mailgun-mailer for Mailgun.

  * If you want to send emails asynchronously:

    1. Install the messenger component by running composer require messenger;
    2. Add 'Symfony\Component\Mailer\Messenger\SendEmailMessage': amqp to the
       config/packages/messenger.yaml file under framework.messenger.routing
       and replace amqp with your transport name of choice.

  * Read the documentation at https://symfony.com/doc/master/mailer.html

                        
 Database Configuration 
                        

  * Modify your DATABASE_URL config in .env

  * Configure the driver (mysql) and
    server_version (5.7) in config/packages/doctrine.yaml

              
 How to test? 
              

  * Write test cases in the tests/ folder
  * Run php bin/phpunit

```


## Create the database ##

1/ Create `.env.dev` file

2/ Setup DB parameters: `DATABASE_URL=mysql://homestead:secret@127.0.0.1:3306/symfony4?serverVersion=5.7`

3/ `php bin/console doctrine:database:create`


## User authentication ##

### Make user entity ###

`php bin/console make:user`

The result:

```
The name of the security user class (e.g. User) [User]:
 > 

 Do you want to store user data in the database (via Doctrine)? (yes/no) [yes]:
 > 

 Enter a property name that will be the unique "display" name for the user (e.g. email, username, uuid) [email]:
 > 

 Will this app need to hash/check user passwords? Choose No if passwords are not needed or will be checked/hashed by some other system (e.g. a single sign-on server).

 Does this app need to hash/check user passwords? (yes/no) [yes]:
 > 

 created: src/Entity/User.php
 created: src/Repository/UserRepository.php
 updated: src/Entity/User.php
 updated: config/packages/security.yaml

           
  Success! 
           

 Next Steps:
   - Review your new App\Entity\User class.
   - Use make:entity to add more fields to your User entity and then run make:migration.
   - Create a way to authenticate! See https://symfony.com/doc/current/security.html

```

### Database migration ###

Create the migration:

`php bin/console make:migration`

Result:

```
           
  Success! 
           

 Next: Review the new migration "src/Migrations/Version20200122192702.php"
 Then: Run the migration with php bin/console doctrine:migrations:migrate
 See https://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html
 ```

Update database schema:

`php bin/console doctrine:migrations:migrate`

### Install Faker ###

We will need this to generate fake data:

`composer require fzaninotto/faker`

### Install orm-fixtures ###

We will need this package to populate database

`composer require orm-fixtures --dev`

Create user fixture 

`php bin/console make:fixture` 

```
The class name of the fixtures to create (e.g. AppFixtures):
 > UserFixture

 created: src/DataFixtures/UserFixture.php

           
  Success! 
           

 Next: Open your new fixtures class and start customizing it.
 Load your fixtures by running: php bin/console doctrine:fixtures:load
 Docs: https://symfony.com/doc/master/bundles/DoctrineFixturesBundle/index.html

```

Load fixtures to the database:

`php bin/console doctrine:fixtures:load`

### Generate the authentication ###

```
 php bin/console make:auth

 What style of authentication do you want? [Empty authenticator]:
  [0] Empty authenticator
  [1] Login form authenticator
 > 1

 The class name of the authenticator to create (e.g. AppCustomAuthenticator):
 > LoginFormAuthenticator    

 Choose a name for the controller class (e.g. SecurityController) [SecurityController]:
 > 

 Do you want to generate a '/logout' URL? (yes/no) [yes]:
 > 

 created: src/Security/LoginFormAuthenticator.php
 updated: config/packages/security.yaml
 created: src/Controller/SecurityController.php
 created: templates/security/login.html.twig

           
  Success! 
           

 Next:
 - Customize your new authenticator.
 - Finish the redirect "TODO" in the App\Security\LoginFormAuthenticator::onAuthenticationSuccess() method.
 - Review & adapt the login template: templates/security/login.html.twig.


```

PS: we updated the user fixture to use UserPasswordEncoderInterface

## Security: Access Control ##

There are two main places where you can deny access: 

1/ security.yaml 

``` 
    access_control:
        - { path: ^/dashboard, roles: ROLE_USER }
        - { path: ^/admin, roles: ROLE_ADMIN }
        # - { path: ^/profile, roles: ROLE_USER }
```

2/ Inside controller!

Inside the controller method:

```
    $this->denyAccessUnlessGranted('ROLE_ADMIN');
```

Or using annotation above the controller `method` or `class`!!! 

```
    @IsGranted({"ROLE_ADMIN", "ROLE_USER"})
```

```
    @IsGranted("ROLE_ADMIN")
```

Inside twig: 

```
    {% if is_granted('ROLE_USER') %} 
        // show something
    {% endif %}
```

##### More with access control: ##### 
In this example we grant access to non authenticated users for one page which is `/login`:

``` 
    access_control:
        - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/change-password, roles: IS_AUTHENTICATED_FULLY }
        - { path: ^/, roles: IS_AUTHENTICATED_REMEMBERED }
```

#### Role hierarchy ####

``` 
security:
    role_hierarchy:
        ROLE_ADMIN: [ROLE_ADMIN_COMMENT, ROLE_ADMIN_ARTICLE]
        ROLE_USER: [ROLE_ADMIN_ARTICLE]
```

#### Impersonation (switch_user) ####

We need to add `switch_user: true` and `ROLE_ALLOWED_TO_SWITCH` to the role hierarchy

```
security:
    firewalls:
        main:
            switch_user: true
```

```
security:
    role_hierarchy:
        ROLE_ADMIN: [ROLE_ADMIN_COMMENT, ROLE_ADMIN_ARTICLE, ROLE_ALLOWED_TO_SWITCH]
```

Then we need to go to: `http://symfony-app.test?_switch_user=mail_address@example.com`

To go back to the real user: `http://symfony-app.test?_switch_user=_exit`

We put a big banner on top when we're switched to stay careful when using impersonation:

```
    {% if is_granted('ROLE_PREVIOUS_ADMIN') %}
        <div class="alert alert-warning" style="margin-bottom: 0;">
            You are currently switched to this user.
            <a href="{{ path('app_homepage', {'_switch_user': '_exit'}) }}">Exit Impersonation</a>
        </div>
    {% endif %}
```

## Fetch the User Object ##

Inside controller:

``` $this->getUser() ```
In php for example, we can't use autocomplete for this. That's why we can make our controller extends a new `BaseController` that has a `return type User` for `getUser()` function.

```
<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class BaseController extends AbstractController
{
    protected function getUser(): User
    {
        return parent::getUser();
    }
}

```

Inside twig:

`{{app.user.firstName}}`

Inside a service:

We use Security!

``` 
namespace App\Service;

use Symfony\Component\Security\Core\Security;

class ServiceExample
{
    private $security;
    
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function doSomething()
    {
        $user = $this->security->getUser();
        
        // do something ...
    }
}

```
