<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    private $faker;
    private $encoder;


    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->faker = Factory::create();
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User();

            $user->setEmail($this->faker->email);

            if ($i === 0) {
                $user->setEmail('hello@oussama.tn');
            }

            $password = $this->encoder->encodePassword($user, 'secret');
            $user->setPassword($password);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
