<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted({"ROLE_ADMIN", "ROLE_USER"})
 */
class TestController extends AbstractController
{

    /**
     * @Route("/test/attach-role", name="test_attach_role")
     */
    public function attachRole(UserRepository $userRepository, EntityManagerInterface $manager)
    {
        $user = $userRepository->findOneBy(['email' => 'hello@oussama.tn']);

        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);
        $manager->flush();

        return $this->json([
            'user' => $user,
            'roles' => $user->getRoles(),
        ]);
    }


    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }

}
